# pixel-editor-master

Written on Javascript, CSS and HTML.
Contains - color picker, painting template and download painting button.

- Color can be chosen throught all variations of colors' hue values - 0-360 (all RGB colors without change of saturation or light).
- Top color display changes color and name after color is chosen. The name comes with 'rgb(,,)' template and is formatted to show HEX.
- Grid is updated by entering integer value to input fields (if one input is affected, the other takes its value. Checkers are placed to accept only numbers)
- If grid elements number is too high (above 50 for example) it takes more time to load. So a loader is placed to be displayed while the grid is generated.
- Coloring the grid blocks is made by clicking on them OR by dragging the mouse with selected color.
- Finaly when the painting is done, it can be downloaded as a PNG image.

Note:
 - The app is also flexible and prepared to display rectangles if required.
Just update startingValues.js width and height from 500x500 to 500x300 or 300x500 for example.
The color picker height corresponds to the grid height (if the grid is rectangular, the picker will have the rectangular height).


`npm i`
generate node-modules

`npm start`
to start project localy on port localhost:1234 (default)

`npm run deploy`
to load dist folder with correct paths of assets for production deploy



To Fix:
- incorrect color on fast mouse movement
- visible lines on downloaded image file when canvasWidth/numeberOfBlocks is not an integer number.

To Do:
- responsive version