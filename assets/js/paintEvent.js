let mousePressed = false;

// on mouse down
export const paintingStart = function (e) {
    if(e.target.classList.contains('painting-block')) {
        e.target.style.backgroundColor = document.getElementById('chosen').style.backgroundColor;
        mousePressed = true;
    }
}
// on mouse up
export const paintingEnd = function (e) {
    mousePressed = false;
}
// on mouse move
export const paintingAction = function (e) {
    if(mousePressed && e.target.classList.contains('painting-block')) {
        e.target.style.backgroundColor = document.getElementById('chosen').style.backgroundColor;
    }
}
