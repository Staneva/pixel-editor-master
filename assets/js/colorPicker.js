// get color picker block coordinates - its top position inside the page.
let parentTop = document.getElementById('picker').offsetTop;
let activatePicker = false;
let newhue = 0;

// on mouse down
export const colorPickingStart = function(e, hueStart) {
    if(e.target.classList.contains('movable')) {
        activatePicker = true;
        newhue = hueStart;
        /* TODO - set color on click somewhere on the color-picker bar*/
    }
};

// on mouse up
export const colorPickingEnd = function(e) {
    activatePicker = false;
};


// on mouse move
export const colorPickingAction = function(e, heightEl) {
    if(activatePicker) {
        //execute logic only if color-select is targeted
        let barPart = heightEl/360; //get size of 1/360 part of picker height
        let moveElTop = e.clientY - parentTop; // calculate selector element top position
        // keep element position inside parent and hue values between 0-360
        if( moveElTop < 0) {
            document.getElementById('color-select').style.top = "0px";
            newhue = 0;
        } else if ( moveElTop >= heightEl ) {
            document.getElementById('color-select').style.top = heightEl + "px";
            newhue = 360;
        } else {
            document.getElementById('color-select').style.top = moveElTop + 'px';
            // increase or decrease hue according to movement direction
            newhue = (newhue*barPart <= moveElTop) ? ++newhue : --newhue;
        }

        distributeColors(newhue);
    }

};

export const distributeColors = function(hue) {
    // following design - hue is reversed - from top->bottom is 360->0
    // put color to arrows and chosen color display
    document.getElementById('left-arrow').style.borderColor = 'hsl('+ (360-hue) +', 100%, 50%)';
    document.getElementById('right-arrow').style.borderColor = 'hsl('+ (360-hue) +', 100%, 50%)';
    document.getElementById('chosen').style.backgroundColor = 'hsl('+ (360-hue) +', 100%, 50%)';
    document.getElementById('chosen').innerHTML = "<span>"+ chosenColorLabel(document.getElementById('chosen').style.backgroundColor) +"</span>";
}

// get the RGB value and turn it to HEX
function chosenColorLabel(colorRGB) {
    var colorLabel = "#";
    var hexValue = 0;
    // cut all string and leave only the numbers
    var colors = colorRGB.substring(4, colorRGB.length-1).replace(/ /g, '').split(',');
    for(var i = 0; i < colors.length; i++) {
        // turn the "numbers" to integer to convert them in hexadecimal
        hexValue = parseInt(colors[i]).toString(16);
        // display 0 in front of hex if only 1 digit is displayed - for full color hex experience
        colorLabel += (hexValue.length == 1) ? "0"+hexValue : hexValue;
    }
    return colorLabel;
}
