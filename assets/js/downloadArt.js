let canvas = document.getElementById("myImageCanvas");
let blocks = document.getElementsByClassName("painting-block");
let link = document.createElement('a'); // new link element to download element
let innerCanvas = 0;
export const downloadPic = function(e) {
    if(e.target.id == 'download') {
        // if clicked on download button, start painting on canvas all blocks we already have from the grid
        for(var i = 0; i < blocks.length; i++) {
            // create single particle in the canvas for every block we have
            innerCanvas = canvas.getContext("2d");
            // if a block is not colored make it white
            innerCanvas.fillStyle = (blocks[i].style.backgroundColor) ? blocks[i].style.backgroundColor : "#ffffff";
            // position new painted block to canvas - x, y, width, height (x,y position from canvas)
            innerCanvas.fillRect(blocks[i].offsetLeft, blocks[i].offsetTop, blocks[i].offsetWidth, blocks[i].offsetHeight);
        }
        // after our masterpiece is ready, add it to link lement and download it
        link.download = 'myInnerPollock.png';
        link.href = canvas.toDataURL()
        link.click();
    }
}
