export const paintingGrid = function(gridWidth, gridHeight) {

    var paining = document.getElementById('painting');
    var numRows = document.getElementById('rows');
    var numColumns = document.getElementById('columns');

    generateGrid(numRows.value);

    //add events to both inputs
    document.querySelectorAll('.number-blocks').forEach(item => {
        //prevent writing value if it is not a number
        //check if only numbers are entered (and 0)
        //allow buttons for correction
        item.addEventListener('keydown', event => {
            if( !(Number(event.key) || event.key == '0' ||
                  event.key == 'Backspace' || event.key == 'ArrowLeft' ||
                  event.key == 'ArrowLeft' || event.key == 'Delete') ) {

                      event.preventDefault();

            } else {
                //include loader - when number of blocks is too high and takes more time to generate.
                paining.classList.add("loader");
            };
        })
        //copy value for rows and blocks no matter where it is entered
        item.addEventListener('keyup', event => {
            numRows.value = numColumns.value = item.value;
            painting.innerHTML = '';//clear previous grid
            generateGrid(item.value);
            paining.classList.remove("loader");
        })
    })

    function generateGrid(numBlocks) {
          var sizeW = gridWidth/numBlocks; //get block width
          var sizeH = gridHeight/numBlocks; //get block height
          var paintingBlock = "<div class='painting-block' style='width: "+sizeW+"px;'></div>";
          var paintingRow = "<div class='painting-row' style='height: "+sizeH+"px;'>"+paintingBlock.repeat(numBlocks)+"</div>";
          for(var rows = 0; rows < numBlocks; rows++) {
              painting.innerHTML += paintingRow;
          }
    }

}
