// add option for rectangle version.
// color picker height depends on the grid height.
export const gridWidth = 500;
export const gridHeight = 500;
export const hueStart = 167;
export const colorSelectPosition = (gridHeight/2 - document.getElementById('color-select').offsetHeight);
document.getElementById('painting').style.width = gridWidth;
document.getElementById('painting').style.height = gridHeight;
document.getElementById('picker').style.height = gridHeight;
document.getElementById('color-select').style.top = colorSelectPosition;

// TODO - add responsivenes to app - window resize
