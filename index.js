import './index.css';
import * as colorPicker from "./assets/js/colorPicker";
import { paintingGrid } from "./assets/js/paintingGrid";
import * as paintEvent from "./assets/js/paintEvent";
import { gridWidth, gridHeight, hueStart } from "./assets/js/startingValues";
import { downloadPic } from "./assets/js/downloadArt";

// start values on page load.
colorPicker.distributeColors(hueStart);
paintingGrid(gridWidth, gridHeight);

// mouse actions
document.addEventListener('mousedown', function(e) {
    colorPicker.colorPickingStart(e, hueStart);
    paintEvent.paintingStart(e);
    downloadPic(e);
});

document.addEventListener('mouseup', function(e) {
    colorPicker.colorPickingEnd(e);
    paintEvent.paintingEnd(e);
});

document.addEventListener('mousemove', function(e) {
    colorPicker.colorPickingAction(e, gridHeight);
    paintEvent.paintingAction(e);
});
